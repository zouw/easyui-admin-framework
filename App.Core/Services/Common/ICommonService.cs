﻿using App.Core.Domain.BaseObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Services
{
    public partial interface ICommonService : IDependency
    {
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="where">默认查询条件</param>
        /// <param name="page">页码</param>
        /// <param name="rows">分页大小</param>
        /// <param name="sort">排序字段</param>
        /// <param name="order">ASC/DESC</param>
        /// <param name="total">总记录数</param>
        /// <param name="filterRules">easyui列查询条件</param>
        /// <param name="includes">要包含的子对象</param>
        /// <returns></returns>
        List<T> GetPageRecords<T>(string where, int page, int rows, string sort, string order, out int total, string filterRules = "", string[] includes = null);
        /// <summary>
        /// 获取指定条件的数据记录
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="where"></param>
        /// <param name="orderby"></param>
        /// <returns></returns>
        List<T> GetRecords<T>(string where, string orderby);
        /// <summary>
        /// 获取指定条件的数据记录
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="where"></param>
        /// <param name="sort"></param>
        /// <param name="order"></param>
        /// <param name="filterRules"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        List<T> GetRecords<T>(string where, string sort, string order, string filterRules = "", string[] includes = null);
    }
}
