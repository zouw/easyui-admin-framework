﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.DataAccess;
using App.Core.Domain;

namespace App.Services.Impls
{
    public partial class CommonServiceImpl : AppServiceBase,ICommonService
    {
        public List<T> GetPageRecords<T>(string where, int page, int rows, string sort, string order, out int total, string filterRules = "", string[] includes = null)
        {
            using (var db = new Db())
            {
                var query = db.QueryByESql<T>(where, page, rows, sort, order, out total,
                    filterRules, includes);

                return query.ToList();
            }
        }

        public List<T> GetRecords<T>(string where, string orderby)
        {
            using (var db = new Db())
            {
                return db.QueryByESql<T>(where, orderby).ToList();
            }
        }

        public List<T> GetRecords<T>(string where, string sort, string order, string filterRules = "", string[] includes = null)
        {
            using (var db = new Db())
            {
                return db.QueryByESql<T>(where, "c." + sort + " " + order, filterRules, includes).ToList();
            }
        }
    }
}
