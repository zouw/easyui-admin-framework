﻿using System;
using System.Threading.Tasks;
using App.Core.Domain.BaseObject;
using App.Core.Dtos;
using App.Core.DataAccess;
using System.Data.Entity;
using App.Core.Domain;
using Omu.ValueInjecter;
using System.Linq;
using EntityFramework.Extensions;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace App.Services.Impls
{
    public partial class AddressServiceImpl : IAddressService
    {

        public async Task<OperationResult> CreateAddress(AddressDto dto)
        {
            using (var db = new Db())
            {
                if (await db.Address.CountAsync(t => t.Consignee == dto.Consignee) > 0)
                {
                    return new OperationResult() { success = false, message = string.Format("名为【{0}】的收货地址已经存在！", dto.Consignee) };
                }
                var entity = new Address();
                entity.InjectFrom(dto);

                db.Address.Add(entity);
                var result = await db.SaveChangesAsync();

                return new OperationResult() { success = result > 0 };
            }
        }


        public async Task<OperationResult> UpdateAddress(Guid id, AddressDto dto)
        {
            using (var db = new Db())
            {
                var entity = await db.Address.FindAsync(id);
                if (entity == null)
                {
                    return new OperationResult() { success = false, message = string.Format("找不到该记录Id为{0}的记录", id) };
                }

                entity.InjectFrom(dto);

                var result = await db.SaveChangesAsync();

                return new OperationResult() { success = result > 0, message= result == 0 ? "数据没有任何改变":"" };
            }
        }

        public async Task<Address> GetAddressAsync(Guid id)
        {
            using (var db = new Db())
            {
                return await db.Address.FindAsync(id);
            }
        }

        public async Task<OperationResult> DeleteAsync(Guid id)
        {
            using (var db = new Db())
            {
                await db.Address.Where(s => s.Id == id).DeleteAsync();

                return new OperationResult();
            }
        }

        public async Task<Address> GetAddressAsync(Expression<Func<Address, bool>> pression)
        {
            using (var db = new Db())
            {
                return await db.Address.FirstOrDefaultAsync(pression);
            }
        }

        public async Task<List<AddressDto>> GetUserAddress(Guid userId)
        {
            using (var db = new Db())
            {
                var data = await db.Address.Where(s=>s.CreatorUserId == userId).ToListAsync();
                return data.Select(s => new AddressDto
                {
                    Consignee = s.Consignee,
                    ConsigneeAddress = s.ConsigneeAddress,
                    ConsigneeCity = s.ConsigneeCity,
                    ConsigneeDistrict = s.ConsigneeDistrict,
                    ConsigneeEmail = s.ConsigneeEmail,
                    ConsigneePhone = s.ConsigneePhone,
                    ConsigneeProvince = s.ConsigneeProvince,
                    ConsigneeSex = s.ConsigneeSex
                }).ToList();
            }
        }
    }
}
