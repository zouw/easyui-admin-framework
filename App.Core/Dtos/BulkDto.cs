﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Dtos
{
    /// <summary>
    /// 批量添加、修改、删除
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BulkDto<T>
    {
        public BulkDto()
        {
            InsertedRows = new List<T>();
            UpdatedRows = new List<T>();
            DeletedRows = new List<T>();
        }

        public List<T> InsertedRows { get; set; }
        public List<T> UpdatedRows { get; set; }
        public List<T> DeletedRows { get; set; }
    }

    /// <summary>
    /// 多个键值
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BulkKeyDto<T>
    {
        public BulkKeyDto()
        {
            KeyList = new List<T>();
        }

        public List<T> KeyList { get; set; }
    }

    public class BulkEntityDto {
        public Guid? Id { get; set; }
    }
}
