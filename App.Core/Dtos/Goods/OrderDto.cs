﻿using System;
using App.Core.Domain.Auditing;

namespace App.Core.Dtos
{
    public partial class OrderDto
    {
        public string Mobile { get; set; }
        public string MobileCode { get; set; }
        public string RedeemCode { get; set; }
        public System.String Consignee { get; set; }
        public System.String ConsigneePhone { get; set; }
        public System.String ConsigneeSex { get; set; }
        public System.String ConsigneeEmail { get; set; }
        public System.String ConsigneeProvince { get; set; }
        public System.String ConsigneeCity { get; set; }
        public System.String ConsigneeDistrict { get; set; }
        public System.String ConsigneeAddress { get; set; }
    }
}