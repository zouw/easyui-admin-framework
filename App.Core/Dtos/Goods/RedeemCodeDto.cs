﻿using App.Core.Domain.BaseObject;
using System;

namespace App.Core.Dtos.Goods
{
    public class RedeemCodeDto : BaseModel
    {
        public string OrderSN { get; set; }
        public string UserName { get; set; }
        public string PackageName { get; set; }
        public string CodeNumber { get; set; }
        public bool IsExchange { get; set; }
        public DateTime? ExchangeTime { get; set; }
        public Guid? ExchangeUserId { get; set; }
        public Guid? ExchangeOrderId { get; set; }
        public int TotalRedeemCode { get; set; }
    }
}
