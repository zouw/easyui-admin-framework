﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core
{
    public class AppConsts
    {
        /// <summary>
        /// 系统版本号
        /// </summary>
        public const string AppVersion = "2.1";

        /// <summary>
        /// 注册用户默认角色
        /// </summary>
        public const string SettingKeyUserDefaultRole = "user.defaultrole";
    }
}
