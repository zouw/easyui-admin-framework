﻿using System.Collections.Generic;

namespace App.Core.Domain.BaseObject
{
    public class GridResult
    {
        public long total { get; set; }
        public object rows { get; set; }
    }

    public class GridResult<T>
    {
        public long total { get; set; }
        public List<T> rows { get; set; }
    }
}
