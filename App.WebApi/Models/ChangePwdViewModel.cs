﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.WebApi.ViewModels
{
    public class ChangePwdViewModel
    {
        public string OldPassword { get; set; }
        public string Password { get; set; }
    }
}