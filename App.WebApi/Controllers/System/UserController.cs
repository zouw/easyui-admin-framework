﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using App.Core.Domain;
using App.Core.Domain.BaseObject;
using App.Core.Dtos;
using App.Core.Helpers;
using App.Services;
using App.WebApi.ViewModels;

namespace App.WebApi.Apis
{
    public class UserController : AppApiControllerBase
    {
        public readonly ICommonService _commonService;
        public readonly IUserService _userService;
        public readonly ISystemService _systemService;
        public UserController(ICommonService CommonService, IUserService UserService, ISystemService systemService)
        {
            _commonService = CommonService;
            _userService = UserService;
            _systemService = systemService;
        }

        [Route("api/Users")]
        public GridResult GetUser(int page, int rows, string sort = "CreationTime", string order = "asc", Guid? teamId = null, string filterRules = "")
        {
            int total;
            var where = "1=1";
            if (teamId != null)
                where += " and c.Team.Id=GUID'" + teamId + "'";

            var query = _commonService.GetPageRecords<User>(where, page, rows, sort, order, out total,
                filterRules, new[] { "Team", "Roles" });

            return new GridResult { total = total, rows = query };
        }

        [HttpPost]
        [Route("api/GetAllMenuByUserId/{id}")]
        public Guid[] GetAllMenuByUserId(Guid id) {
            return _systemService.GetAllMenuByUserId(id).Select(s => s.Id).ToArray();
        }

        [HttpPut]
        [Route("api/Users/{id}")]
        public async Task<IHttpActionResult> PutUser(Guid id, UserDto dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _userService.UpdateUser(id, dto);

            return Ok(result);
        }

        [HttpPost]
        [Route("api/Users")]
        public async Task<IHttpActionResult> PostUser(UserDto dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _userService.CreateUserAsync(dto);

            return Ok(result);
        }

        [HttpDelete]
        [Route("api/Users")]
        public async Task<IHttpActionResult> DeleteUser()
        {
            await _userService.DeleteAsync(GetRequestDeleteModels());

            return Ok(new OperationResult());
        }

        [Route("api/change-user-pwd")]
        public async Task<IHttpActionResult> ChangeUserPassword(ChangePwdDto dto)
        {
            var result = await _userService.ChangeUserPassword(dto);

            return Ok(result);
        }

        [Route("api/Teams")]
        public List<TeamTree> GetTeam()
        {
            var data = _userService.GeTeams();
            var result = new List<TeamTree>();
            Recursion(new TeamTree { id = null, children = new List<TeamTree>() }, result, data.ToList());
            return result;
        }

        [HttpPut]
        [Route("api/Teams/{id}")]
        public async Task<IHttpActionResult> PutTeam(Guid id, TeamDto dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _userService.UpdateTeam(id, dto);

            return Ok(result);
        }

        [HttpPost]
        [Route("api/Teams")]
        public async Task<IHttpActionResult> PostTeam(TeamDto dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _userService.CreateTeam(dto);

            return Ok(result);
        }

        [HttpDelete]
        [Route("api/Teams")]
        public async Task<IHttpActionResult> DeleteTeam(DeleteViewModel model)
        {
            await _userService.DeleteTeamAsync(model.Id);

            return Ok(new OperationResult { success = true });
        }

        #region Helpers
        //递归
        private void Recursion(TeamTree parentNode, IList<TeamTree> result, IList<Team> list)
        {
            foreach (var item in from c in list where c.ParentId == parentNode.id select c)
            {
                var child = new TeamTree { id = item.Id, text = item.TeamName, ParentId = item.ParentId, TeamName = item.TeamName, TeamDesc = item.TeamDesc, expanded = true, children = new List<TeamTree>() };
                if (item.ParentId == null)
                {
                    result.Add(child);
                }
                else
                {
                    child.leaf = true;
                    parentNode.children.Add(child);
                }
                Recursion(child, result, list);
            }
        }
        #endregion
    }
}